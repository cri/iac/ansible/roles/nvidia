# nvidia

This role aims to install NVIDIA drivers and container runtime on a given host.

## Usage

`requirements.yml`:
```yaml
roles:
  - name: forge.nvidia
    src: git+https://gitlab.cri.epita.fr/cri/iac/ansible/roles/nvidia.git
    version: 0.1.0
```

Example of playbook:
```yaml
---

- hosts: all
  roles:
    - {role: forge.nvidia, tags: ['nvidia']}
```

With its variables file:
```yaml
nvidia_version: 525
```
